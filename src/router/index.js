import Vue from 'vue'
import VueRouter from 'vue-router'

import Layout from '@/layout';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: '/emerAss/index',  //急診護理評估
    component: Layout,
    children: [
      {
        path: 'emerAss/index/:hsp/:crtno/:actno',
        name: 'emerAss',
        component: () => import('@/views/emerAss/index')
      }
    ]
  },
  {
    path: '/emerDayRec',
    redirect: '/emerDayRec/index',   //急診護理記錄
    component: Layout,
    children: [
      {
        path: 'index/:hspnme/:crtno/:actno',
        name: 'emerDayRec',
        component: () => import('@/views/emerDayRec/index')
      }
    ]
  },
  {
    path: '/noteResume',                //照會履歷
    redirect: '/noteResume/index',
    component: Layout,
    children: [
      {
        path: 'index/:hspnme/:crtno/:crtseq',
        name: 'noteResume',
        component: () => import('@/views/noteResume/index')
      }
    ]
  },
  {
    path: '/oprTraint',                    //急診約束
    redirect: '/oprTraint/index',
    component: Layout,
    children: [
      {
        path: 'index/:hsp/:crtno/:actno/:opid',
        name: 'oprTraint',
        component: () => import('@/views/oprTraint/index')
      }
    ]
  },
  {
    path: '/oprGcsRec',               //急診意識評估紀錄
    redirect: '/oprGcsRec/index',
    component: Layout,
    children: [
      {
        path: 'index/:hsp/:crtno/:actno',
        name: 'oprGcsRec',
        component: () => import('@/views/oprGcsRec/index')
      }
    ]
  },
  {
    path: '/ntrRec',              //營養照顧紀錄
    redirect: '/ntrRec/index',
    component: Layout,
    children: [
      {
        path: 'index/:crtno',
        name: 'ntrRec',
        component: () => import('@/views/ntrRec/index')
      }
    ]
  },
  {
    path: '/oprTransfer',            //急診轉歸動向
    redirect: '/oprTransfer/index',
    component: Layout,
    children: [
      {
        path: 'index/:area/:crtno/:actno',
        name: 'oprTransfer',
        component: () => import('@/views/oprTransfer/index')
      }
    ]
  },
  {
    path: '/oprLifeRec',   //急診生命徵象
    redirect: '/oprLifeRec/index',
    component: Layout,
    children: [
      {
        path: 'index/:organ/:crtno/:actno',
        name: 'oprLifeRec',
        component: () => import('@/views/oprLifeRec/index.vue')
      }
    ]
  },
  {
    path: '/anePreRec',              //麻醉前評估單
    redirect: '/anePreRec/index/',
    component: Layout,
    children: [
      {
        path: 'index/:hspnme/:opcrtno/:opdate/:scrno',
        name: 'anePreRec',
        component: () => import('@/views/anePreRec/index.vue')
      }
    ]
  },
  {
    path: '/PostOperativeTransfer',             //手術後轉 ICU 交班單
    redirect: '/PostOperativeTransfer/index/',
    component: Layout,
    children: [
      {
        path: 'index/:hspnme/:opcrtno/:opdate/:scrno',
        name: 'PostOperativeTransfer',
        component: () => import('@/views/postOperativeTransfer/index.vue')
      }
    ]
  },
  {
    path: '/404',
    component: () => import('@/views/errorPage/404'),
    hidden: true
  },
  {
    path: '*',
    redirect: '/404',
    component: () => import('@/views/errorPage/404'),
    hidden: true
  }
];

const router = new VueRouter({
  routes
});

export default router;
